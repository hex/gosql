package main

import(
  _"github.com/go-sql-driver/mysql"
  "database/sql"
  "fmt"
  "log"
  "github.com/dustin/go-humanize"
  "strconv"
  )

func main() {
  db, err := sql.Open("mysql", "pdf:wtARWzZznTTq6R9R@tcp(176.31.171.199:3306)/pdf")
  if err != nil {
      log.Println(err)
  }
  defer db.Close()
  err = db.Ping()
  if err == nil {
	   //fmt.Println("funciona")
     var id, maquina, estat, time_estat, size string
     rows, err := db.Query("select id, maquina, estat, time_estat, size from paquets where estat < 5 order by estat asc")

     if err != nil {
       log.Fatal(err)
     }

     for rows.Next() {
       err = rows.Scan(&id, &maquina, &estat, &time_estat, &size)
	fmt.Print(string(time_estat) + " > " + string(id))
	tamany, err := strconv.ParseUint(string(size), 0, 64)

	if err != nil {
		log.Fatal(err)
	}

	switch (string(estat)) {
		case "0":
			fmt.Println(" -> Creant un nou paquet.");
			break;
		case "1":
			fmt.Println(" (" + humanize.Bytes(tamany) + ") Esperant: Disponible per distribuir.");
			break;
		case "2":
			fmt.Println(" (" + humanize.Bytes(tamany) + ") -> Enviant a linea " + string(maquina) + ".");
			break;
		case "3":
			fmt.Println(" (" + humanize.Bytes(tamany) + ") Eperant: Pujable desde linea" + string(maquina) + ".");
			break;
		case "4":
			fmt.Println(" (" + humanize.Bytes(tamany) + ") -> Pujant desde linea" + string(maquina) + ".");
			break;
	}

       if err != nil {
         log.Fatal(err)
       }
     }

     if err = rows.Err(); err != nil {
       log.Fatal(err)
     }

     defer rows.Close()

     /*
     Scan(&a, &b, &c)
     fmt.Println(str[0])
     */
  } else {
    log.Println("no funciona")
    log.Println(err)
  }
  //fmt.Println("fin")
}
