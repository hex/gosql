/* DaytimeServer
 */
package main

import (
	"fmt"
	"net"
	"os"
)

func main() {

	fmt.Println("Running...")
	service := ":80"
	tcpAddr, err := net.ResolveTCPAddr("tcp", service)
	checkError(err)

	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

		//conn, err := listener.Accept()
		//checkError(err)
		listener.Close()
}

func checkError(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
		os.Exit(1)
	}
}
