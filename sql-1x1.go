package main

import(
  _"github.com/go-sql-driver/mysql"
  "database/sql"
  "fmt"
  "log"
  )

func main() {
  db, err := sql.Open("mysql", "root:admin1991@/golang")
  if err != nil {
      log.Println(err)
  }
  defer db.Close()
  err = db.Ping()
  if err == nil {
	   fmt.Println("funciona")
     var str string
     err = db.QueryRow("select a from testing").Scan(&str)
     if err != nil && err != sql.ErrNoRows {
       log.Println(err)
     }
     fmt.Println("S'esta imprimint: " + str)
  } else {
    log.Println("no funciona")
    log.Println(err)
  }
  fmt.Println("fin")
}
