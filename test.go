package main

import(
  _"github.com/go-sql-driver/mysql"
  "database/sql"
  "fmt"
  "log"
  "github.com/dustin/go-humanize"
  "strconv"
  "time"
  gc "github.com/rthornton128/goncurses"

)

func main() {
  var mensajes []string

  loop := true

  stdscr, err := gc.Init()
  if err != nil {
    log.Fatal(err)
  }
  defer gc.End()
  gc.Cursor(0)
  gc.Echo(false)



db, err := sql.Open("mysql", "pdf:wtARWzZznTTq6R9R@tcp(176.31.171.199:3306)/pdf")
if err != nil {
      log.Println(err)
}
defer db.Close()

  for (loop) { // LOOP
    stdscr.Clear()
    stdscr.Border(gc.ACS_VLINE, gc.ACS_VLINE, gc.ACS_HLINE, gc.ACS_HLINE, gc.ACS_ULCORNER, gc.ACS_URCORNER, gc.ACS_LLCORNER, gc.ACS_LRCORNER)
      err = db.Ping()
      if err == nil {
	       //fmt.Println("funciona")
         var id, maquina, estat, time_estat, size string
         rows, err := db.Query("select id, maquina, estat, time_estat, size from paquets where estat < 5 order by id desc")

         if err != nil {
           log.Fatal(err)
         }

         for rows.Next() {
           err = rows.Scan(&id, &maquina, &estat, &time_estat, &size)
           tamany, err := strconv.ParseUint(string(size), 0, 64)

           if err != nil {
             log.Fatal(err)
           }

           cadena := ""
           switch (string(estat)) {
             case "0":
              mensajes = append(mensajes, " -> Creant un nou paquet.");
			        break;

		         case "1":
            cadena = string(time_estat) + " > " + string(id) + " (" + humanize.Bytes(tamany) + ") Esperant: Disponible per distribuir."
             mensajes = append(mensajes, cadena)
			        break;

		         case "2":
            cadena = string(time_estat) + " > " + string(id) + " (" + humanize.Bytes(tamany) + ") -> Enviant a linea " + string(maquina) + "."
             mensajes = append(mensajes, cadena)
			        break;

		         case "3":
             cadena = string(time_estat) + " > " + string(id) + " (" + humanize.Bytes(tamany) + ") Eperant: Pujable desde linea" + string(maquina) + "."
             mensajes = append(mensajes, cadena)
			        break;

		         case "4":
             cadena = string(time_estat) + " > " + string(id) + " (" + humanize.Bytes(tamany) + ") -> Pujant desde linea" + string(maquina) + "."
             mensajes = append(mensajes, cadena)
			        break;
        }

       if err != nil {
         log.Fatal(err)
       }
     }

     if err = rows.Err(); err != nil {
       log.Fatal(err)
     }

     defer rows.Close()

     /*
     Scan(&a, &b, &c)
     fmt.Println(str[0])
     */
     posicion := 1
     for _, a := range mensajes {
       stdscr.MovePrint(posicion, 4, a)
       //fmt.Println(a)
       //stdscr.Println(a)
       posicion++
     }
     stdscr.Refresh()

     posicion = 1
     mensajes = mensajes[:0]

  } else {
    fmt.Println(err)
  }

  time.Sleep(time.Millisecond * 10000)

} // LOOP
  //fmt.Println("fin")
}
