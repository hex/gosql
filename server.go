/* DaytimeServer
 */
package main

import (
	"fmt"
	"net"
	"os"
	"bufio"
	"net/textproto"
)

func main() {

	fmt.Println("Abriendo conexion puerto 1200.")
	service := ":1200"
	tcpAddr, err := net.ResolveTCPAddr("tcp4", service)
	checkError(err)

	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

		conn, err := listener.Accept()
		if err != nil {
		}
		reader := bufio.NewReader(conn)
		tp := textproto.NewReader(reader)
		fmt.Println("> Se ha establecido una conexion.")
		conn.Write([]byte("Hola benvingut a la meva maquina!"))
	for {

		conn.Write([]byte("Escriu una opcio:\n"))
		conn.Write([]byte("1 - Saludar.\n"))
		conn.Write([]byte("2 - Marchar.\n"))
		line, err := tp.ReadLine()
		if err != nil {
			break
		}
		switch (line) {
			case "1":
				conn.Write([]byte("Hola telnet!!\n"))
				fmt.Println("El cas 1 ha sigut solicitat")
				break
			case "2":
				conn.Write([]byte("Deixant la conexio.\n"))
				fmt.Println("El cas 2 ha sigut solicitat")
				os.Exit(0)
				break
			default:
				conn.Write([]byte("No se que intentes dirme.\n"))
				fmt.Println("Algu ha escrit: " + line)

		}
	}
		conn.Close()
}

func checkError(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
		os.Exit(1)
	}
}
