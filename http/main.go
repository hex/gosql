package hello

import (
	"net/http"
	"github.com/gin-gonic/gin"
)

func init() {
	r := gin.New()
	r.GET("/", func(c *gin.Context) {
		c.String(200, "Hello World")
	})

	r.GET("/ping", func(c *gin.Context) {
		c.String(200, "pong")
	})

	http.Handle("/", r)
}
