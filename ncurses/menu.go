package main

import(
	gc "github.com/rthornton128/goncurses"
	"log"
)

func main() {
	stdscr, err := gc.Init()
	if err != nil {
		log.Fatal(err)
	}

	defer gc.End()
	gc.Cursor(0)
	gc.Echo(false);
	stdscr.Border(gc.ACS_VLINE, gc.ACS_VLINE, gc.ACS_HLINE, gc.ACS_HLINE, gc.ACS_ULCORNER, gc.ACS_URCORNER, gc.ACS_LLCORNER, gc.ACS_LRCORNER)
	stdscr.Refresh()
	stdscr.Keypad(true)
	for {
		ch := stdscr.GetChar()
		switch gc.Key(ch) {
			case 'q':
				return
			case gc.KEY_LEFT:
				stdscr.Clear()
				stdscr.Refresh()
	stdscr.Border(gc.ACS_VLINE, gc.ACS_VLINE, gc.ACS_HLINE, gc.ACS_HLINE, gc.ACS_ULCORNER, gc.ACS_URCORNER, gc.ACS_LLCORNER, gc.ACS_LRCORNER)
				stdscr.MovePrint(3, 4, "ESQUERRA")
			case gc.KEY_RIGHT:
				stdscr.Clear()
				stdscr.Refresh()
	stdscr.Border(gc.ACS_VLINE, gc.ACS_VLINE, gc.ACS_HLINE, gc.ACS_HLINE, gc.ACS_ULCORNER, gc.ACS_URCORNER, gc.ACS_LLCORNER, gc.ACS_LRCORNER)
				stdscr.MovePrint(3, 4,	"DRETA")
			case gc.KEY_UP:
				stdscr.Clear()
				stdscr.Refresh()
	stdscr.Border(gc.ACS_VLINE, gc.ACS_VLINE, gc.ACS_HLINE, gc.ACS_HLINE, gc.ACS_ULCORNER, gc.ACS_URCORNER, gc.ACS_LLCORNER, gc.ACS_LRCORNER)
				stdscr.MovePrint(3, 4, "UP")
			case gc.KEY_DOWN:
				stdscr.Clear()
				stdscr.Refresh()
	stdscr.Border(gc.ACS_VLINE, gc.ACS_VLINE, gc.ACS_HLINE, gc.ACS_HLINE, gc.ACS_ULCORNER, gc.ACS_URCORNER, gc.ACS_LLCORNER, gc.ACS_LRCORNER)
				stdscr.MovePrint(3, 4, "ABALL")	
			case gc.KEY_RETURN, gc.KEY_ENTER, gc.Key('\r'):
				stdscr.Clear()
				stdscr.Refresh()
	stdscr.Border(gc.ACS_VLINE, gc.ACS_VLINE, gc.ACS_HLINE, gc.ACS_HLINE, gc.ACS_ULCORNER, gc.ACS_URCORNER, gc.ACS_LLCORNER, gc.ACS_LRCORNER)
			stdscr.MovePrint(3, 4, "INTRO")
			default:
				//stdscr.MovePrint(3, 4, "Character pressed = %c", ch)
		}
	}

}
